// const requestURL = './motif.json';
// const request = new XMLHttpRequest();
// request.open('GET', requestURL);
// request.responseType = 'json';
// request.send();

const fieldset1 = document.getElementsByClassName('form-part2-fieldset1');
const legend = document.getElementsByClassName('fieldset1-legend');
const activeSpan = document.getElementsByClassName('form-part1-fieldset2');

fetch('./motif.json', {
    method: 'GET'
})
    .then((response) => response.json())
    .then((data) => membersFromJson(data))
    .catch((error) => console.error(error))

function membersFromJson(jsonObj) {
    let myH3 = document.createElement('h3');
    myH3.textContent = jsonObj["topTitle"];
    
    legend[0].appendChild(myH3);
    
    let myMembers = jsonObj['members'];
    myMembers.forEach(element => {
        let myH4 = document.createElement('h4');
        myH4.textContent = element.name + " : " + element.title;

        fieldset1[0].appendChild(myH4);
        
        let membersOptions = element.options;
        membersOptions.forEach(element => {
            let myUl1 = document.createElement('ul');
            let myLi1 = document.createElement('li');
            let label = document.createElement('label');
            label.htmlFor = element.op_id;
            label.textContent = element.op_label;
            
            let checkbox = document.createElement('input');
            checkbox.type = "checkbox";
            checkbox.name = element.op_label;
            checkbox.value = element.op_id;
            checkbox.id = element.op_id

            fieldset1[0].appendChild(myUl1)
            myUl1.appendChild(myLi1)
            myLi1.appendChild(checkbox)
            myLi1.appendChild(label)
        })
    });
}

// request.onload = function() {
//     let members = request.response;
//     titleFromJson(members);
//     membersFromJson(members);
// }